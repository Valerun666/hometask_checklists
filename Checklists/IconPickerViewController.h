//
//  IconPickerViewController.h
//  Checklists
//
//  Created by Valerun on 16.03.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IconPickerViewController;

@protocol IconPickerViewControllerDelegate <NSObject>

- (void)iconPicker:(IconPickerViewController *)picker
       didPickIcon:(NSString *)icon;

@end

@interface IconPickerViewController : UITableViewController

@property (nonatomic, weak) id <IconPickerViewControllerDelegate> delegate;

@end

//
//  ChecklistItem.m
//  Checklists
//
//  Created by Valerun on 01.03.15.
//  Copyright (c) 2015 Valerun. All rights reserved.
//

#import "ChecklistItem.h"
#import "DataModel.h"
#import <UIKit/UIKit.h>

@implementation ChecklistItem

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super init])) {
        self.text = [aDecoder decodeObjectForKey:@"Text"];
        self.checked = [aDecoder decodeBoolForKey:@"Checked"];
        self.dueDate = [aDecoder decodeObjectForKey:@"DueDate"];
        self.shouldRemind = [aDecoder decodeBoolForKey:@"ShouldRemind"];
        self.itemId = [aDecoder decodeIntegerForKey:@"ItemID"];
    }
    
    return self;
}

- (instancetype)init
{
    if ((self = [super init])) {
        self.itemId = [DataModel nextChecklistItemId];
    }
    
    return self;
}

- (void)toggleChecked
{
    self.checked = !self.checked;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.text forKey:@"Text"];
    [aCoder encodeBool:self.checked forKey:@"Checked"];
    [aCoder encodeObject:self.dueDate forKey:@"DueDate"];
    [aCoder encodeBool:self.shouldRemind forKey:@"ShouldRemind"];
    [aCoder encodeInteger:self.itemId forKey:@"ItemID"];
}

- (void)scheduleNotification
{
    UILocalNotification *existingNotification = [self notificationForThisItem];
    
    if (existingNotification != nil) {
        [[UIApplication sharedApplication] cancelLocalNotification:existingNotification];
    }
    if (self.shouldRemind && [self.dueDate compare:[NSDate date]] != NSOrderedAscending) {
        
        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
        [localNotification setFireDate:self.dueDate];
        [localNotification setTimeZone:[NSTimeZone defaultTimeZone]];
        [localNotification setAlertBody:self.text];
        [localNotification setSoundName:UILocalNotificationDefaultSoundName];
        localNotification.hasAction = NO;
        localNotification.userInfo = @{ @"ItemID" : @(self.itemId) };
        
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }
}

- (UILocalNotification *)notificationForThisItem
{
    NSArray *allNotifications = [[UIApplication sharedApplication] scheduledLocalNotifications];
    
    for (UILocalNotification *notification in allNotifications) {
        NSNumber *number = [notification.userInfo objectForKey:@"ItemID"];
        
        if (number != nil && [number integerValue] == self.itemId) {
            return notification;
        }
    }
    return nil;
}

- (NSComparisonResult)compare:(ChecklistItem *)otherItem
{
    return [self.dueDate compare:otherItem.dueDate];
}


- (void)dealloc
{
    UILocalNotification *existingNotification = [self notificationForThisItem];
    
    if (existingNotification != nil) {
        [[UIApplication sharedApplication] cancelLocalNotification:existingNotification];
    }
}

@end
